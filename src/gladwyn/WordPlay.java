package gladwyn;

import java.util.HashMap;
import java.util.Map;

/*
Gladwyn's java class.
*/
public class WordPlay {
	  private static final Map<Character, Character> encodeMap = new HashMap<Character, Character>();
	  private static final Map<Character, Character> decodeMap = new HashMap<Character, Character>();
  
  static {
	  Character[] f = {'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'};
	  Character[] t = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
	  for (int i=0; i<f.length; i++) {
		  encodeMap.put(t[i], f[i]);
		  decodeMap.put(f[i], t[i]);
	  }
  }
  
  public static void main(String[] args) {
	  String test = "O QD WKOSSOQFZ QZ DOFR-WTFRTK HXMMSTL QFR EQF LGSCT ZIOL OF STLL ZIQF ZVG DOFXZTL";
	  testDecode(test);
	  
	  System.out.println();
	  
	  test = "I LIKE MOM AND DAD";
	  testEncode(test);
	  
  }
  
  private static void testDecode(String test) {
	  StringBuilder result = new StringBuilder();
	  
	  System.out.println("Decode. Input is: ");
	  System.out.println(test);
	  System.out.println();
	  System.out.println("Working ... ");

	  for (int i=0; i<test.length(); i++) {
		  Character f = test.charAt(i);
		  Character t = decodeMap.get(f);
		  t = t == null ? f : t;
		  result.append(t);
		  System.out.println(f + " ===> " + t);
	  }
	  
	  System.out.println();
	  System.out.println("Yes, I fount it.");
	  System.out.println("Result is: ");
	  System.out.println(result);
  }
  
  private static void testEncode(String test) {
	  StringBuilder result = new StringBuilder();
	  
	  System.out.println("Encode. Input is: ");
	  System.out.println(test);
	  System.out.println();
	  System.out.println("Working ... ");

	  for (int i=0; i<test.length(); i++) {
		  Character f = test.charAt(i);
		  Character t = decodeMap.get(f);
		  t = t == null ? f : t;
		  result.append(t);
		  System.out.println(f + " ===> " + t);
	  }
	  
	  System.out.println();
	  System.out.println("Yes, I fount it.");
	  System.out.println("Result is: ");
	  System.out.println(result);
  }
}
